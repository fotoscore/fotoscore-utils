import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fotoscore_utils",
    version="1.0.3",
    author="aavetisyan, norman",
    author_email="aavetisyan@fotoscore.de, norman@fotoscore.de",
    description="Package with python utility scripts for FotoScore.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=["fotoscore_utils"],
    python_requires='>=3.6',
)
