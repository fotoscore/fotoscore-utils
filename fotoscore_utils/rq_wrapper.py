import pika
from functools import partial
import uuid
import time
import bson.json_util as json
import traceback
import sys, pickle


def serialize_err(e):
  return dict(error=pickle.dumps(e), traceback=traceback.format_exc())

class RQWrapper:
  def __init__(self, namespace, port):
    self.connection_consume = pika.BlockingConnection(pika.ConnectionParameters(host="localhost", port=port))
    self.connection_produce = pika.BlockingConnection(pika.ConnectionParameters(host="localhost", port=port))
    self.channel_consume = self.connection_consume.channel()
    self.channel_produce = self.connection_produce.channel()
    self.namespace = namespace

  def __del__(self):
    self.connection_consume.close()
    self.connection_produce.close()

  def lazy_task(self, body, service, task):
    service = f"{self.namespace}.{service}"
    if isinstance(body, dict) or isinstance(body, list):
      body = json.dumps(body)
    assert isinstance(body, str), "Only strings sendable"

    channel = self.channel_produce

    # assert exchange and queue exists
    assert channel.exchange_declare(exchange=service, exchange_type="topic", passive=True), "Exchange does not exist"
    assert channel.queue_declare(f"{service}.{task}", passive=True), "Qeue does not exist"

    channel.basic_publish(exchange=service, routing_key=task, body=body)


  def eager_task(self, body, service, task, timeout=5000):
    service = f"{self.namespace}.{service}"
    if isinstance(body, dict) or isinstance(body, list):
      body = json.dumps(body)
    assert isinstance(body, str), "Only strings sendable"

    channel = self.channel_produce

    result = channel.queue_declare(queue="", auto_delete=True, exclusive=True)
    reply_queue = result.method.queue
    corr_id = str(uuid.uuid4())

    # publish task to queue
    channel.exchange_declare(exchange=f"{service}.direct", exchange_type="direct", durable=False)
    channel.queue_bind(queue=reply_queue, exchange=f"{service}.direct", routing_key=reply_queue)

    assert channel.exchange_declare(exchange=service, exchange_type="topic", passive=True), "Exchange does not exist"
    assert channel.queue_declare(f"{service}.{task}", passive=True), "Qeue does not exist"
    channel.basic_publish(exchange=service, routing_key=task,
        properties=pika.BasicProperties(reply_to=reply_queue, correlation_id=corr_id), body=body)

    res = None
    def on_response(ch, method, properties, body):
      nonlocal res
      if corr_id == properties.correlation_id:
        res = json.loads(body)

    channel.basic_consume(queue=reply_queue, on_message_callback=on_response, auto_ack=True)

    start_time = time.time()
    while res is None:
      self.connection_produce.process_data_events()
      if time.time() - start_time > timeout/1000.0:
        raise TimeoutError

    if "error" in res:
      raise pickle.loads(res["error"]) # reraise

    return res


  def eager_response(self, channel, service, properties, body):
    if body is None:
      body = {}
    if isinstance(body, dict) or isinstance(body, list):
      body = json.dumps(body)
    assert isinstance(body, str), "Only strings sendable"

    service_direct = f"{service}.direct"
    channel.basic_publish(exchange=service_direct, routing_key=str(properties.reply_to),
        properties=pika.BasicProperties(correlation_id=properties.correlation_id), body=body)


  def consume_tasks(self, service, mappings):
    service = f"{self.namespace}.{service}"
    def wrapper(fn, ch, method, properties, data: str):
      try:
        data = json.loads(data)
        res = fn(data)
      except Exception as e:
        print(traceback.format_exc())
        res = serialize_err(e)
      finally:
        if properties is not None:
          self.eager_response(channel, service, properties, res)

    channel = self.channel_consume
    channel.exchange_declare(exchange=service, exchange_type='topic')

    for task, fn in mappings.items():
      queue = f"{service}.{task}"
      channel.queue_declare(queue, exclusive=False)
      channel.queue_bind(queue=queue, exchange=service, routing_key=task)
      channel.basic_consume(queue=queue, on_message_callback=partial(wrapper, fn), auto_ack=True)

    channel.start_consuming()
